# uni-ts-register
[![npm version](https://img.shields.io/npm/v/uni-ts-register)](https://www.npmjs.com/package/uni-ts-register)

Run typescript with any register
When create a lib project with bin and need run typescript file (.ts), using `uni-ts-register`, no matter which register the lib user installs

## Support Register
- [@swc-node/register](https://github.com/Brooooooklyn/swc-node)
- [esbuild-register](https://github.com/egoist/esbuild-register)
- [sucrase/register](https://github.com/alangpierce/sucrase)
- [ts-node/register](https://github.com/TypeStrong/ts-node)

## Usage
Create a file to run `main.ts`
``` js
require('uni-ts-register')
require('main.ts')
```
