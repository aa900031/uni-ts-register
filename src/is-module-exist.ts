export const isModuleExist = (
  name: string,
) => {
  try {
    require.resolve(name)
  } catch (err) {
    if ('code' in (err as any) && (err as any).code === 'MODULE_NOT_FOUND') {
      return false
    }
  }
  return true
}
