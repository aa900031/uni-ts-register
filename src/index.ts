import { isModuleExist } from './is-module-exist'

const SUPPORT_MODULES = [
  {
    name: 'sucrase',
    path: 'sucrase/register',
  },
  {
    name: '@swc-node/register',
    path: '@swc-node/register',
  },
  {
    name: 'esbuild-register',
    path: 'esbuild-register',
  },
  {
    name: 'ts-node',
    path: 'ts-node/register',
  },
]

;(() => {
  const result = SUPPORT_MODULES.some(({ path }) => {
    if (isModuleExist(path)) {
      require(path)
      return true
    }
    return false
  })
  result === false && new Error(`Please install registers (ex: ${SUPPORT_MODULES.map(({ name }) => name)})`)
})()
