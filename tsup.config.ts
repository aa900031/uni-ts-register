import { defineConfig } from 'tsup'

export default defineConfig({
  entryPoints: [
    'src/index.ts',
  ],
  format: [
    'cjs',
    'esm',
  ],
  sourcemap: true,
  clean: true,
  minify: true,
})
